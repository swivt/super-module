// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_model.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

abstract class _$UserModelCWProxy {
  UserModel activeCountries(int? activeCountries);

  UserModel address(String? address);

  UserModel avatar(String? avatar);

  UserModel classroom(Classroom? classroom);

  UserModel companyValuationPoint(
      List<CompanyValuationPoint>? companyValuationPoint);

  UserModel createdAt(String? createdAt);

  UserModel currentStudying(String? currentStudying);

  UserModel email(String? email);

  UserModel emailVerified(bool? emailVerified);

  UserModel gameAvatar(GameAvatar? gameAvatar);

  UserModel growthTrend(List<GrowthTrend>? growthTrend);

  UserModel id(String? id);

  UserModel language(String? language);

  UserModel name(String? name);

  UserModel nickname(String? nickname);

  UserModel panel(String? panel);

  UserModel phone(String? phone);

  UserModel phoneVerified(bool? phoneVerified);

  UserModel points(int? points);

  UserModel rank(int? rank);

  UserModel role(String? role);

  UserModel score(double? score);

  UserModel shotsDiscovered(int? shotsDiscovered);

  UserModel social(bool? social);

  UserModel socialLogin(bool? socialLogin);

  UserModel status(String? status);

  UserModel studentGrades(List<double>? studentGrades);

  UserModel subscribedCountries(List<String>? subscribedCountries);

  UserModel totalCampnayValuation(double? totalCampnayValuation);

  UserModel totalCash(double? totalCash);

  UserModel totalcompanyBudget(double? totalcompanyBudget);

  UserModel totalcompanyReputation(double? totalcompanyReputation);

  UserModel totalemployNumber(double? totalemployNumber);

  UserModel totalemploySatisfaction(double? totalemploySatisfaction);

  UserModel updatedAt(String? updatedAt);

  UserModel username(String? username);

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `UserModel(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// UserModel(...).copyWith(id: 12, name: "My name")
  /// ````
  UserModel call({
    int? activeCountries,
    String? address,
    String? avatar,
    Classroom? classroom,
    List<CompanyValuationPoint>? companyValuationPoint,
    String? createdAt,
    String? currentStudying,
    String? email,
    bool? emailVerified,
    GameAvatar? gameAvatar,
    List<GrowthTrend>? growthTrend,
    String? id,
    String? language,
    String? name,
    String? nickname,
    String? panel,
    String? phone,
    bool? phoneVerified,
    int? points,
    int? rank,
    String? role,
    double? score,
    int? shotsDiscovered,
    bool? social,
    bool? socialLogin,
    String? status,
    List<double>? studentGrades,
    List<String>? subscribedCountries,
    double? totalCampnayValuation,
    double? totalCash,
    double? totalcompanyBudget,
    double? totalcompanyReputation,
    double? totalemployNumber,
    double? totalemploySatisfaction,
    String? updatedAt,
    String? username,
  });
}

/// Proxy class for `copyWith` functionality. This is a callable class and can be used as follows: `instanceOfUserModel.copyWith(...)`. Additionally contains functions for specific fields e.g. `instanceOfUserModel.copyWith.fieldName(...)`
class _$UserModelCWProxyImpl implements _$UserModelCWProxy {
  final UserModel _value;

  const _$UserModelCWProxyImpl(this._value);

  @override
  UserModel activeCountries(int? activeCountries) =>
      this(activeCountries: activeCountries);

  @override
  UserModel address(String? address) => this(address: address);

  @override
  UserModel avatar(String? avatar) => this(avatar: avatar);

  @override
  UserModel classroom(Classroom? classroom) => this(classroom: classroom);

  @override
  UserModel companyValuationPoint(
          List<CompanyValuationPoint>? companyValuationPoint) =>
      this(companyValuationPoint: companyValuationPoint);

  @override
  UserModel createdAt(String? createdAt) => this(createdAt: createdAt);

  @override
  UserModel currentStudying(String? currentStudying) =>
      this(currentStudying: currentStudying);

  @override
  UserModel email(String? email) => this(email: email);

  @override
  UserModel emailVerified(bool? emailVerified) =>
      this(emailVerified: emailVerified);

  @override
  UserModel gameAvatar(GameAvatar? gameAvatar) => this(gameAvatar: gameAvatar);

  @override
  UserModel growthTrend(List<GrowthTrend>? growthTrend) =>
      this(growthTrend: growthTrend);

  @override
  UserModel id(String? id) => this(id: id);

  @override
  UserModel language(String? language) => this(language: language);

  @override
  UserModel name(String? name) => this(name: name);

  @override
  UserModel nickname(String? nickname) => this(nickname: nickname);

  @override
  UserModel panel(String? panel) => this(panel: panel);

  @override
  UserModel phone(String? phone) => this(phone: phone);

  @override
  UserModel phoneVerified(bool? phoneVerified) =>
      this(phoneVerified: phoneVerified);

  @override
  UserModel points(int? points) => this(points: points);

  @override
  UserModel rank(int? rank) => this(rank: rank);

  @override
  UserModel role(String? role) => this(role: role);

  @override
  UserModel score(double? score) => this(score: score);

  @override
  UserModel shotsDiscovered(int? shotsDiscovered) =>
      this(shotsDiscovered: shotsDiscovered);

  @override
  UserModel social(bool? social) => this(social: social);

  @override
  UserModel socialLogin(bool? socialLogin) => this(socialLogin: socialLogin);

  @override
  UserModel status(String? status) => this(status: status);

  @override
  UserModel studentGrades(List<double>? studentGrades) =>
      this(studentGrades: studentGrades);

  @override
  UserModel subscribedCountries(List<String>? subscribedCountries) =>
      this(subscribedCountries: subscribedCountries);

  @override
  UserModel totalCampnayValuation(double? totalCampnayValuation) =>
      this(totalCampnayValuation: totalCampnayValuation);

  @override
  UserModel totalCash(double? totalCash) => this(totalCash: totalCash);

  @override
  UserModel totalcompanyBudget(double? totalcompanyBudget) =>
      this(totalcompanyBudget: totalcompanyBudget);

  @override
  UserModel totalcompanyReputation(double? totalcompanyReputation) =>
      this(totalcompanyReputation: totalcompanyReputation);

  @override
  UserModel totalemployNumber(double? totalemployNumber) =>
      this(totalemployNumber: totalemployNumber);

  @override
  UserModel totalemploySatisfaction(double? totalemploySatisfaction) =>
      this(totalemploySatisfaction: totalemploySatisfaction);

  @override
  UserModel updatedAt(String? updatedAt) => this(updatedAt: updatedAt);

  @override
  UserModel username(String? username) => this(username: username);

  @override

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `UserModel(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// UserModel(...).copyWith(id: 12, name: "My name")
  /// ````
  UserModel call({
    Object? activeCountries = const $CopyWithPlaceholder(),
    Object? address = const $CopyWithPlaceholder(),
    Object? avatar = const $CopyWithPlaceholder(),
    Object? classroom = const $CopyWithPlaceholder(),
    Object? companyValuationPoint = const $CopyWithPlaceholder(),
    Object? createdAt = const $CopyWithPlaceholder(),
    Object? currentStudying = const $CopyWithPlaceholder(),
    Object? email = const $CopyWithPlaceholder(),
    Object? emailVerified = const $CopyWithPlaceholder(),
    Object? gameAvatar = const $CopyWithPlaceholder(),
    Object? growthTrend = const $CopyWithPlaceholder(),
    Object? id = const $CopyWithPlaceholder(),
    Object? language = const $CopyWithPlaceholder(),
    Object? name = const $CopyWithPlaceholder(),
    Object? nickname = const $CopyWithPlaceholder(),
    Object? panel = const $CopyWithPlaceholder(),
    Object? phone = const $CopyWithPlaceholder(),
    Object? phoneVerified = const $CopyWithPlaceholder(),
    Object? points = const $CopyWithPlaceholder(),
    Object? rank = const $CopyWithPlaceholder(),
    Object? role = const $CopyWithPlaceholder(),
    Object? score = const $CopyWithPlaceholder(),
    Object? shotsDiscovered = const $CopyWithPlaceholder(),
    Object? social = const $CopyWithPlaceholder(),
    Object? socialLogin = const $CopyWithPlaceholder(),
    Object? status = const $CopyWithPlaceholder(),
    Object? studentGrades = const $CopyWithPlaceholder(),
    Object? subscribedCountries = const $CopyWithPlaceholder(),
    Object? totalCampnayValuation = const $CopyWithPlaceholder(),
    Object? totalCash = const $CopyWithPlaceholder(),
    Object? totalcompanyBudget = const $CopyWithPlaceholder(),
    Object? totalcompanyReputation = const $CopyWithPlaceholder(),
    Object? totalemployNumber = const $CopyWithPlaceholder(),
    Object? totalemploySatisfaction = const $CopyWithPlaceholder(),
    Object? updatedAt = const $CopyWithPlaceholder(),
    Object? username = const $CopyWithPlaceholder(),
  }) {
    return UserModel(
      activeCountries: activeCountries == const $CopyWithPlaceholder()
          ? _value.activeCountries
          // ignore: cast_nullable_to_non_nullable
          : activeCountries as int?,
      address: address == const $CopyWithPlaceholder()
          ? _value.address
          // ignore: cast_nullable_to_non_nullable
          : address as String?,
      avatar: avatar == const $CopyWithPlaceholder()
          ? _value.avatar
          // ignore: cast_nullable_to_non_nullable
          : avatar as String?,
      classroom: classroom == const $CopyWithPlaceholder()
          ? _value.classroom
          // ignore: cast_nullable_to_non_nullable
          : classroom as Classroom?,
      companyValuationPoint:
          companyValuationPoint == const $CopyWithPlaceholder()
              ? _value.companyValuationPoint
              // ignore: cast_nullable_to_non_nullable
              : companyValuationPoint as List<CompanyValuationPoint>?,
      createdAt: createdAt == const $CopyWithPlaceholder()
          ? _value.createdAt
          // ignore: cast_nullable_to_non_nullable
          : createdAt as String?,
      currentStudying: currentStudying == const $CopyWithPlaceholder()
          ? _value.currentStudying
          // ignore: cast_nullable_to_non_nullable
          : currentStudying as String?,
      email: email == const $CopyWithPlaceholder()
          ? _value.email
          // ignore: cast_nullable_to_non_nullable
          : email as String?,
      emailVerified: emailVerified == const $CopyWithPlaceholder()
          ? _value.emailVerified
          // ignore: cast_nullable_to_non_nullable
          : emailVerified as bool?,
      gameAvatar: gameAvatar == const $CopyWithPlaceholder()
          ? _value.gameAvatar
          // ignore: cast_nullable_to_non_nullable
          : gameAvatar as GameAvatar?,
      growthTrend: growthTrend == const $CopyWithPlaceholder()
          ? _value.growthTrend
          // ignore: cast_nullable_to_non_nullable
          : growthTrend as List<GrowthTrend>?,
      id: id == const $CopyWithPlaceholder()
          ? _value.id
          // ignore: cast_nullable_to_non_nullable
          : id as String?,
      language: language == const $CopyWithPlaceholder()
          ? _value.language
          // ignore: cast_nullable_to_non_nullable
          : language as String?,
      name: name == const $CopyWithPlaceholder()
          ? _value.name
          // ignore: cast_nullable_to_non_nullable
          : name as String?,
      nickname: nickname == const $CopyWithPlaceholder()
          ? _value.nickname
          // ignore: cast_nullable_to_non_nullable
          : nickname as String?,
      panel: panel == const $CopyWithPlaceholder()
          ? _value.panel
          // ignore: cast_nullable_to_non_nullable
          : panel as String?,
      phone: phone == const $CopyWithPlaceholder()
          ? _value.phone
          // ignore: cast_nullable_to_non_nullable
          : phone as String?,
      phoneVerified: phoneVerified == const $CopyWithPlaceholder()
          ? _value.phoneVerified
          // ignore: cast_nullable_to_non_nullable
          : phoneVerified as bool?,
      points: points == const $CopyWithPlaceholder()
          ? _value.points
          // ignore: cast_nullable_to_non_nullable
          : points as int?,
      rank: rank == const $CopyWithPlaceholder()
          ? _value.rank
          // ignore: cast_nullable_to_non_nullable
          : rank as int?,
      role: role == const $CopyWithPlaceholder()
          ? _value.role
          // ignore: cast_nullable_to_non_nullable
          : role as String?,
      score: score == const $CopyWithPlaceholder()
          ? _value.score
          // ignore: cast_nullable_to_non_nullable
          : score as double?,
      shotsDiscovered: shotsDiscovered == const $CopyWithPlaceholder()
          ? _value.shotsDiscovered
          // ignore: cast_nullable_to_non_nullable
          : shotsDiscovered as int?,
      social: social == const $CopyWithPlaceholder()
          ? _value.social
          // ignore: cast_nullable_to_non_nullable
          : social as bool?,
      socialLogin: socialLogin == const $CopyWithPlaceholder()
          ? _value.socialLogin
          // ignore: cast_nullable_to_non_nullable
          : socialLogin as bool?,
      status: status == const $CopyWithPlaceholder()
          ? _value.status
          // ignore: cast_nullable_to_non_nullable
          : status as String?,
      studentGrades: studentGrades == const $CopyWithPlaceholder()
          ? _value.studentGrades
          // ignore: cast_nullable_to_non_nullable
          : studentGrades as List<double>?,
      subscribedCountries: subscribedCountries == const $CopyWithPlaceholder()
          ? _value.subscribedCountries
          // ignore: cast_nullable_to_non_nullable
          : subscribedCountries as List<String>?,
      totalCampnayValuation:
          totalCampnayValuation == const $CopyWithPlaceholder()
              ? _value.totalCampnayValuation
              // ignore: cast_nullable_to_non_nullable
              : totalCampnayValuation as double?,
      totalCash: totalCash == const $CopyWithPlaceholder()
          ? _value.totalCash
          // ignore: cast_nullable_to_non_nullable
          : totalCash as double?,
      totalcompanyBudget: totalcompanyBudget == const $CopyWithPlaceholder()
          ? _value.totalcompanyBudget
          // ignore: cast_nullable_to_non_nullable
          : totalcompanyBudget as double?,
      totalcompanyReputation:
          totalcompanyReputation == const $CopyWithPlaceholder()
              ? _value.totalcompanyReputation
              // ignore: cast_nullable_to_non_nullable
              : totalcompanyReputation as double?,
      totalemployNumber: totalemployNumber == const $CopyWithPlaceholder()
          ? _value.totalemployNumber
          // ignore: cast_nullable_to_non_nullable
          : totalemployNumber as double?,
      totalemploySatisfaction:
          totalemploySatisfaction == const $CopyWithPlaceholder()
              ? _value.totalemploySatisfaction
              // ignore: cast_nullable_to_non_nullable
              : totalemploySatisfaction as double?,
      updatedAt: updatedAt == const $CopyWithPlaceholder()
          ? _value.updatedAt
          // ignore: cast_nullable_to_non_nullable
          : updatedAt as String?,
      username: username == const $CopyWithPlaceholder()
          ? _value.username
          // ignore: cast_nullable_to_non_nullable
          : username as String?,
    );
  }
}

extension $UserModelCopyWith on UserModel {
  /// Returns a callable class that can be used as follows: `instanceOfUserModel.copyWith(...)` or like so:`instanceOfUserModel.copyWith.fieldName(...)`.
  // ignore: library_private_types_in_public_api
  _$UserModelCWProxy get copyWith => _$UserModelCWProxyImpl(this);
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserModel _$UserModelFromJson(Map<String, dynamic> json) => UserModel(
      socialLogin: json['socialLogin'] as bool?,
      totalCampnayValuation:
          (json['totalCampnayValuation'] as num?)?.toDouble(),
      totalCash: (json['totalCash'] as num?)?.toDouble(),
      totalcompanyBudget: (json['totalcompanyBudget'] as num?)?.toDouble(),
      totalcompanyReputation:
          (json['totalcompanyReputation'] as num?)?.toDouble(),
      totalemployNumber: (json['totalemployNumber'] as num?)?.toDouble(),
      totalemploySatisfaction:
          (json['totalemploySatisfaction'] as num?)?.toDouble(),
      currentStudying: json['currentStudying'] as String?,
      points: json['points'] as int?,
      gameAvatar: json['gameAvatar'] == null
          ? null
          : GameAvatar.fromJson(json['gameAvatar'] as Map<String, dynamic>),
      shotsDiscovered: json['shotsDiscovered'] as int?,
      rank: json['rank'] as int?,
      subscribedCountries: (json['subscribedCountries'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      activeCountries: json['activeCountries'] as int?,
      username: json['username'] as String?,
      companyValuationPoint: (json['companyValuationPoint'] as List<dynamic>?)
          ?.map(
              (e) => CompanyValuationPoint.fromJson(e as Map<String, dynamic>))
          .toList(),
      studentGrades: (json['studentGrades'] as List<dynamic>?)
          ?.map((e) => (e as num).toDouble())
          .toList(),
      growthTrend: (json['growthTrend'] as List<dynamic>?)
          ?.map((e) => GrowthTrend.fromJson(e as Map<String, dynamic>))
          .toList(),
      emailVerified: json['emailVerified'] as bool?,
      nickname: json['nickname'] as String?,
      phoneVerified: json['phoneVerified'] as bool?,
      role: json['role'] as String?,
      status: json['status'] as String?,
      social: json['social'] as bool?,
      language: json['language'] as String?,
      id: json['_id'] as String?,
      name: json['name'] as String?,
      email: json['email'] as String?,
      phone: json['phone'] as String?,
      avatar: json['avatar'] as String?,
      address: json['address'] as String?,
      createdAt: json['createdAt'] as String?,
      updatedAt: json['updatedAt'] as String?,
      score: (json['score'] as num?)?.toDouble(),
      classroom: json['classroom'] == null
          ? null
          : Classroom.fromJson(json['classroom'] as Map<String, dynamic>),
      panel: json['panel'] as String?,
    );

Map<String, dynamic> _$UserModelToJson(UserModel instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('gameAvatar', instance.gameAvatar);
  writeNotNull('emailVerified', instance.emailVerified);
  writeNotNull('phoneVerified', instance.phoneVerified);
  writeNotNull('role', instance.role);
  writeNotNull('status', instance.status);
  writeNotNull('social', instance.social);
  writeNotNull('socialLogin', instance.socialLogin);
  writeNotNull('currentStudying', instance.currentStudying);
  writeNotNull('shotsDiscovered', instance.shotsDiscovered);
  writeNotNull('rank', instance.rank);
  writeNotNull('language', instance.language);
  writeNotNull('_id', instance.id);
  writeNotNull('name', instance.name);
  writeNotNull('email', instance.email);
  writeNotNull('nickname', instance.nickname);
  writeNotNull('points', instance.points);
  writeNotNull('subscribedCountries', instance.subscribedCountries);
  writeNotNull('growthTrend', instance.growthTrend);
  writeNotNull('companyValuationPoint', instance.companyValuationPoint);
  writeNotNull('studentGrades', instance.studentGrades);
  writeNotNull('activeCountries', instance.activeCountries);
  writeNotNull('phone', instance.phone);
  writeNotNull('avatar', instance.avatar);
  writeNotNull('address', instance.address);
  writeNotNull('username', instance.username);
  writeNotNull('createdAt', instance.createdAt);
  writeNotNull('updatedAt', instance.updatedAt);
  writeNotNull('score', instance.score);
  writeNotNull('totalCampnayValuation', instance.totalCampnayValuation);
  writeNotNull('totalCash', instance.totalCash);
  writeNotNull('totalcompanyBudget', instance.totalcompanyBudget);
  writeNotNull('totalcompanyReputation', instance.totalcompanyReputation);
  writeNotNull('totalemployNumber', instance.totalemployNumber);
  writeNotNull('totalemploySatisfaction', instance.totalemploySatisfaction);
  writeNotNull('classroom', instance.classroom);
  writeNotNull('panel', instance.panel);
  return val;
}
