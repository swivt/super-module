// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'description_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DescriptionModel _$DescriptionModelFromJson(Map<String, dynamic> json) =>
    DescriptionModel(
      json['forceDescriptionText'] as Map<String, dynamic>,
      json['descriptionText'] as Map<String, dynamic>,
    );

Map<String, dynamic> _$DescriptionModelToJson(DescriptionModel instance) =>
    <String, dynamic>{
      'forceDescriptionText': instance.forceDescriptionText,
      'descriptionText': instance.descriptionText,
    };
