// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'title_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TitleModel _$TitleModelFromJson(Map<String, dynamic> json) => TitleModel(
      json['forceTitleText'] as Map<String, dynamic>,
      json['titleText'] as Map<String, dynamic>,
    );

Map<String, dynamic> _$TitleModelToJson(TitleModel instance) =>
    <String, dynamic>{
      'forceTitleText': instance.forceTitleText,
      'titleText': instance.titleText,
    };
